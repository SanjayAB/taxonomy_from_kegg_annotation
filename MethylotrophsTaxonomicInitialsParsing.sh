#!/bin/sh
#SBATCH --time=02:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --qos=banyuls
#SBATCH --job-name=methy-COM
#SBATCH --error=methy-COM.err
#SBATCH --output=methy-COM.out
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


EC="EC:1.1.1.284 EC:3.1.2.12 EC:1.2.1.2 EC:1.2.1.43 EC:4.1.2.43 EC:5.3.1.27 EC:3.1.3.11 EC:2.7.1.29 EC:3.1.3.3 EC:2.6.1.52 EC:1.1.1.95 EC:5.4.2.11 EC:2.6.1.45 EC:1.1.1.29 EC:2.7.1.165 EC:4.1.3.24 EC:6.2.1.9 EC:1.1.1.37 EC:4.1.1.31 EC:2.7.9.2 EC:1.1.1.244 EC:1.1.2.7 EC:4.4.1.22 EC:1.2.1.46 EC:4.2.1.11"


echo -n "" > tempSAB-COM-allMethylotrophicGenesECs
echo -n "" > COM-Methylotrophy-taxa-initials
for e in ${EC}
	do
		grep ${e} ../COM-final-BLAT-output >> tempSAB-COM-allMethylotrophicGenesECs
	done

cut -f1 tempSAB-COM-allMethylotrophicGenesECs | sort -u > tempSAB-COM-allMethylotrophicGeneContigs

IFS=$'\n'

for con in $(cat tempSAB-COM-allMethylotrophicGeneContigs)
	do
	
		grep ${con} tempSAB-COM-allMethylotrophicGenesECs > tempSAB-COM-EachContigHits
		sort -k12 -r -n tempSAB-COM-EachContigHits | head -1 > tempSAB-COM-ContigTopHit
		cut -f2 tempSAB-COM-ContigTopHit | cut -f1 -d : >> COM-Methylotrophy-taxa-initials
	done
	
