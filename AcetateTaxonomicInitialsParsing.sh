#!/bin/sh
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --qos=banyuls
#SBATCH --job-name=Ace-COM
#SBATCH --error=Ace-COM.err
#SBATCH --output=Ace-COM.out
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


EC="1.2.7.4 1.2.99.2 6.2.1.1"


echo -n "" > tempSAB-COM-allAcetateGenesECs
echo -n "" > COM-Acetate-taxa-initials

for e in ${EC}
	do
		grep ${e} ../COM-final-BLAT-output >> tempSAB-COM-allAcetateGenesECs
	done

cut -f1 tempSAB-COM-allAcetateGenesECs | sort -u > tempSAB-COM-allAcetateGeneContigs

IFS=$'\n'

for con in $(cat tempSAB-COM-allAcetateGeneContigs)
	do
	
		grep ${con} tempSAB-COM-allAcetateGenesECs > tempSAB-COM-Acetate-EachContigHits
		sort -k12 -r -n tempSAB-COM-Acetate-EachContigHits | head -1 > tempSAB-COM-Acetate-ContigTopHit
		cut -f2 tempSAB-COM-Acetate-ContigTopHit | cut -f1 -d : >> COM-Acetate-taxa-initials
	done
	


		
