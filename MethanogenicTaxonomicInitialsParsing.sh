#!/bin/sh
#SBATCH --time=02:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
##SBATCH --qos=banyuls
#SBATCH --job-name=methano-COM
#SBATCH --error=methano-COM.err
#SBATCH --output=methano-COM.out
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


EC="2.8.4.1 1.8.98.1 2.1.1.86 2.3.1.101 3.5.4.27 1.12.98.2 1.12.98.1 1.2.99.5 4.4.1.19 3.1.3.71 4.1.1.79 2.5.1.77 2.7.8.28 2.7.7.68 6.3.2.31"


echo -n "" > tempSAB-COM-allMethanogenicGenesECs
echo -n "" > COM-Methanogenic-taxa-initials
for e in ${EC}
	do
		grep ${e} ../COM-final-BLAT-output >> tempSAB-COM-allMethanogenicGenesECs
	done

cut -f1 tempSAB-COM-allMethanogenicGenesECs | sort -u > tempSAB-COM-allMethanogenicGeneContigs

IFS=$'\n'

for con in $(cat tempSAB-COM-allMethanogenicGeneContigs)
	do
	
		grep ${con} tempSAB-COM-allMethanogenicGenesECs > tempSAB-COM-Methanogenic-EachContigHits
		sort -k12 -r -n tempSAB-COM-Methanogenic-EachContigHits | head -1 > tempSAB-COM-Methanogenic-ContigTopHit
		cut -f2 tempSAB-COM-Methanogenic-ContigTopHit | cut -f1 -d : >> COM-Methanogenic-taxa-initials
	done
	


		