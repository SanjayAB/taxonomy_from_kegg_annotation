#!/bin/sh
#SBATCH --time=02:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
##SBATCH --qos=banyuls
#SBATCH --job-name=Pro-COM
#SBATCH --error=Pro-COM.err
#SBATCH --output=Pro-COM.out
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


EC="6.2.1.1 6.2.1.17 2.8.3.1 6.2.1.13 2.1.3.1 4.1.1.41 5.1.99.1 5.4.99.2 6.2.1.4 6.2.1.5 6.2.1.17 6.2.1.1 1.3.5.1 4.2.1.2 1.1.5.4 1.1.1.37 1.1.1.82 4.1.1.3 6.4.1.1"


echo -n "" > tempSAB-COM-allPropionateGenesECs
echo -n "" > COM-Propionate-taxa-initials
for e in ${EC}
	do
		grep ${e} ../COM-final-BLAT-output >> tempSAB-COM-allPropionateGenesECs
	done

cut -f1 tempSAB-COM-allPropionateGenesECs | sort -u > tempSAB-COM-allPropionateGeneContigs

IFS=$'\n'

for con in $(cat tempSAB-COM-allPropionateGeneContigs)
	do
	
		grep ${con} tempSAB-COM-allPropionateGenesECs > tempSAB-COM-Propionate-EachContigHits
		sort -k12 -r -n tempSAB-COM-Propionate-EachContigHits | head -1 > tempSAB-COM-Propionate-ContigTopHit
		cut -f2 tempSAB-COM-Propionate-ContigTopHit | cut -f1 -d : >> COM-Propionate-taxa-initials
	done
	

