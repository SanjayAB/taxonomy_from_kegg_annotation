#!/bin/sh
#SBATCH --time=02:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
##SBATCH --qos=banyuls
#SBATCH --job-name=Buty-COM
#SBATCH --error=Buty-COM.err
#SBATCH --output=Buty-COM.out
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


EC="2.8.3.8 1.3.1.44 1.3.8.1 4.2.1.17 1.1.1.35 1.1.1.157 2.3.1.9 2.3.3.10 4.1.3.4 6.2.1.1"


echo -n "" > tempSAB-COM-allButyrateGenesECs
echo -n "" > COM-Butyrate-taxa-initials
for e in ${EC}
	do
		grep ${e} ../COM-final-BLAT-output >> tempSAB-COM-allButyrateGenesECs
	done

cut -f1 tempSAB-COM-allButyrateGenesECs | sort -u > tempSAB-COM-allButyrateGeneContigs

IFS=$'\n'

for con in $(cat tempSAB-COM-allButyrateGeneContigs)
	do
	
		grep ${con} tempSAB-COM-allButyrateGenesECs > tempSAB-COM-Butyrate-EachContigHits
		sort -k12 -r -n tempSAB-COM-Butyrate-EachContigHits | head -1 > tempSAB-COM-Butyrate-ContigTopHit
		cut -f2 tempSAB-COM-Butyrate-ContigTopHit | cut -f1 -d : >> COM-Butyrate-taxa-initials
	done
	


		